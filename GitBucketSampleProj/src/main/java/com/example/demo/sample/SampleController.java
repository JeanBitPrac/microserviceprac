package com.example.demo.sample;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SampleController {
	@RequestMapping("Hello")
	public String hello(){
		return "Hello";
	}
}
